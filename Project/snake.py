
import pygame
import random
from enum import Enum
from collections import namedtuple
import numpy as np

pygame.init()
font = pygame.font.Font('Project/fonts/times.ttf',25)

class Direction(Enum):
    RIGHT = 1 
    LEFT = 2
    UP = 3
    DOWN = 4

Point = namedtuple('Point','x,y')

WHITE = (255, 255, 255)
RED = (200,0,0)
GREEN1 = (0, 200, 0)
GREEN2 = (0, 120, 40)
BLACK = (0,0,0)

BLOCK_SIZE = 20
SPEED = 500

class Snake:
    def __init__(self,w=960, h=720):
        self.w = w
        self.h = h

        self.display = pygame.display.set_mode((self.w, self.h))
        pygame.display.set_caption('Snake')
        self.clock = pygame.time.Clock()

        self.Reset()
        


    def Reset(self):
        self.direction = Direction.RIGHT

        self.head = Point(self.w/2,self.h/2)
        self.snake = [self.head,
                      Point(self.head.x - BLOCK_SIZE, self.head.y),
                      Point(self.head.x- (2*BLOCK_SIZE), self.head.y)
                    ]

        self.score = 0
        self.food = None
        self._PlaceFood()
        self.frameIteration = 0

    def _PlaceFood(self):
        x = random.randint(0, (self.w-BLOCK_SIZE)//BLOCK_SIZE) * BLOCK_SIZE
        y = random.randint(0, (self.h-BLOCK_SIZE)//BLOCK_SIZE) * BLOCK_SIZE

        self.food = Point(x,y)
        if self.food in self.snake:
            self._PlaceFood()
    
    def PlayStep(self, action):

        self.frameIteration += 1

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

#            if event.type == pygame.KEYDOWN:
#                if event.key == pygame.K_LEFT and self.direction is not Direction.RIGHT:
#                    self.direction = Direction.LEFT
#                elif event.key == pygame.K_RIGHT and self.direction is not Direction.LEFT:
#                    self.direction = Direction.RIGHT
#                elif event.key == pygame.K_UP and self.direction is not Direction.DOWN:
#                    self.direction = Direction.UP
#                elif event.key == pygame.K_DOWN and self.direction is not Direction.UP:
#                    self.direction = Direction.DOWN

            
        self._Move(action)
        self.snake.insert(0,self.head)

        reward = 0
        game_over = False
        if self.IsCollision() or self.frameIteration > 100 * len(self.snake):
            game_over = True
            reward = -10
            return reward, game_over, self.score

        
        if self.head == self.food:
            self.score += 1
            reward = 10
            self._PlaceFood()
        else:
            self.snake.pop()

        self._UpdateUI()
        self.clock.tick(SPEED)


        return reward, game_over, self.score

    def IsCollision(self, pt=None):
        if pt is None:
            pt = self.head
        if pt.x > self.w - BLOCK_SIZE or pt.x < 0 or pt.y > self.h - BLOCK_SIZE or pt.y < 0:
            return True
        
        if pt in self.snake[1:]:
            return True
        return False

    def _UpdateUI(self):
        self.display.fill(BLACK)

        for pt in self.snake:
            pygame.draw.rect(self.display, GREEN1, pygame.Rect(pt.x,pt.y, BLOCK_SIZE,BLOCK_SIZE))
            pygame.draw.rect(self.display,GREEN2, pygame.Rect(pt.x+4, pt.y+4, 12, 12))

        pygame.draw.rect(self.display, RED, pygame.Rect(self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE))


        text = font.render("Score: " + str(self.score),True, WHITE)
        self.display.blit(text,[0,0])

        pygame.display.flip()

    def _Move(self,action):
        #[straingt, rightTurn, leftTurn]

        directionsClockwise = [Direction.RIGHT, Direction.DOWN, Direction.LEFT, Direction.UP]

        currentDirIdx = directionsClockwise.index(self.direction)

        if np.array_equal(action, [1, 0, 0]):
            newDir = directionsClockwise[currentDirIdx] #Keep the same direction
        elif np.array_equal(action, [0, 1, 0]):
            nextDirectionIdx = (currentDirIdx+1) % len(directionsClockwise)
            newDir = directionsClockwise[nextDirectionIdx] #go right from current position
        else:
            nextDirectionIdx = (currentDirIdx-1) % len(directionsClockwise)
            newDir = directionsClockwise[nextDirectionIdx]
          
        self.direction = newDir
        x=self.head.x
        y=self.head.y
        if self.direction == Direction.RIGHT:
            x+=BLOCK_SIZE
        elif self.direction == Direction.LEFT:
            x-=BLOCK_SIZE
        elif self.direction == Direction.DOWN:
            y+=BLOCK_SIZE
        elif self.direction == Direction.UP:
            y-= BLOCK_SIZE

        self.head = Point(x,y)

#if __name__ == "__main__":
#    game = Snake(w=1280,h=720)
#
#    while True:
#        gameOver, score = game.PlayStep()
#
#        if(gameOver == True):
#            break
#
#    print("Final Score: ", score)
#
#    pygame.quit()