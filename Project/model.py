import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import os


class DeepQNeuralNetwork(nn.Module):
    def __init__(self, inputSize, hiddenSize, outputSize):
        super().__init__()
        self.linearLayerOne = nn.Linear(inputSize, hiddenSize)
        self.linearLayerTwo = nn.Linear(hiddenSize, hiddenSize)
        self.linearLayerThree = nn.Linear(hiddenSize,outputSize)

    def forward(self, x):
        x = F.relu(self.linearLayerOne(x))
        x = F.relu(self.linearLayerTwo(x))
        x = self.linearLayerThree(x)
        return x


    def Save(self,fileName = 'Project/model_proj.pth'):
        modelFolder = './model'
        if not os.path.exists(modelFolder):
            os.makedirs(modelFolder)

        fileName = os.path.join(modelFolder,fileName)
        torch.save(self.state_dict(),fileName)


class QTrainer:
    def __init__(self,model, learningRate, gamma):
        self.learningRate = learningRate
        self.gamma = gamma
        self.model = model

        self.optimizer = optim.Adam(model.parameters(), lr=self.learningRate)
        self.criterion = nn.MSELoss()

    def TrainStep(self, state, action, reward, nextState, gameOver):
        #prepare incoming data -> PyTorch uses tensors 
        state = torch.tensor(state,dtype=torch.float)
        nextState = torch.tensor(nextState,dtype=torch.float)
        action = torch.tensor(action,dtype=torch.long)
        reward = torch.tensor(reward,dtype=torch.float)
        if len(state.shape) == 1:
            # if the data did not come in as a mini batch -> (1, x)
            state = torch.unsqueeze(state, 0)
            nextState = torch.unsqueeze(nextState, 0)
            action = torch.unsqueeze(action, 0)
            reward = torch.unsqueeze(reward, 0)
            gameOver = (gameOver, )

        # predicted Q values with current state
        pred = self.model(state)
        
        # QNew = Reward + Gamma * max(nextPredictedQValue) -> only do this if not gameOver
        # pred.clone()
        # preds = [argmax(action)] = QNew

        target = pred.clone()
        for idx in range(len(gameOver)):
            QNew = reward[idx]
            if not gameOver[idx]:
                QNew = reward[idx] + self.gamma * torch.max(self.model(nextState[idx])) #Calculate new Q values for games that havent ended
            target[idx][torch.argmax(action).item()] = QNew

        self.optimizer.zero_grad()
        loss = self.criterion(target, pred)
        loss.backward()

        self.optimizer.step()


        


        

