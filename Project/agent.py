from ctypes.wintypes import POINT
from multiprocessing.pool import RUN
import torch
import random
import numpy as np
from snake import Snake, Direction, Point, BLOCK_SIZE
from collections import deque
from model import DeepQNeuralNetwork, QTrainer
from plotter import Plot



MAX_MEMORY = 1_000_000
BATCH_SIZE = 1_000
LEARNING_RATE = 0.00025
MODEL_PATH = "Project/model/model_lr_0_0005_2.pth"
RUN_FROM_EXISTING_MODEL = False


class Agent():
    def __init__(self):
        self.numberOfGames = 0
        self.epsilon = 0 #control the randomness
        self.gamma = 0.9 #discount rate
        self.memory = deque(maxlen=MAX_MEMORY) #popleft() automatically
        self.model = DeepQNeuralNetwork(12, 256, 3)
        self.trainer = QTrainer(self.model,LEARNING_RATE,gamma=self.gamma)


    def SetModel(self,modelPath):
        self.model.load_state_dict(torch.load(modelPath))


    def GetState(self,game):
        snakeHead = game.snake[0]

        leftOfHead = Point(snakeHead.x - BLOCK_SIZE, snakeHead.y)
        rightOfHead = Point(snakeHead.x + BLOCK_SIZE, snakeHead.y)
        upOfHead = Point(snakeHead.x, snakeHead.y  - BLOCK_SIZE)
        downOfHead = Point(snakeHead.x, snakeHead.y + BLOCK_SIZE)

        directionLeft = game.direction == Direction.LEFT
        directionRight = game.direction == Direction.RIGHT
        directionUp = game.direction == Direction.UP
        directionDown = game.direction == Direction.DOWN

        state = [
            #Danger Straight
            (directionRight and game.IsCollision(rightOfHead)) or
            (directionLeft and game.IsCollision(leftOfHead)) or
            (directionUp and game.IsCollision(upOfHead)) or
            (directionDown and game.IsCollision(downOfHead)),

            #Danger Right
            (directionRight and game.IsCollision (downOfHead)) or
            (directionDown and game.IsCollision(leftOfHead)) or
            (directionLeft and game.IsCollision(upOfHead)) or
            (directionUp and game.IsCollision(rightOfHead)),

            #Danger Left
            (directionRight and game.IsCollision (upOfHead)) or
            (directionUp and game.IsCollision(leftOfHead)) or
            (directionLeft and game.IsCollision(downOfHead)) or
            (directionDown and game.IsCollision(rightOfHead)),

            #Move Direction
            directionLeft,
            directionRight,
            directionUp,
            directionDown,

            #Food Location
            game.food.x < game.head.x, #food left of head
            game.food.x > game.head.x, #food right of head
            game.food.y < game.head.y, #food up of head
            game.food.y > game.head.y, #food down of head

            len(game.snake) #Snake length
        ]

        return np.array(state,dtype=int)


    def RememberData(self, state, action, reward, nextState, gameOver):
        self.memory.append((state, action, reward, nextState, gameOver)) #popleft if MAX_MEMORY is reached

    def TrainLongMemory(self): #Train NN on a mini batch from memory after a certain game has ended
        if len(self.memory) > BATCH_SIZE:
            miniSample = random.sample(self.memory, BATCH_SIZE) #list of touples 
        else:
            miniSample = self.memory

        states, actions, rewards, nextStates, gameOvers = zip(*miniSample)
        
        self.trainer.TrainStep(states, actions, rewards, nextStates, gameOvers)

    # Train NN on one sample of experience after each game step
    def TrainShortMemory(self, state, action, reward, nextState, gameOver): 
        self.trainer.TrainStep(state, action, reward, nextState, gameOver)

    def GetAction(self,state):
        #random moves: tradeoff between exploration and exploitation
        if not RUN_FROM_EXISTING_MODEL:
            self.epsilon = 320 - self.numberOfGames
        else:
            self.epsilon=-1
        newMove = [0, 0, 0]
        if random.randint(0,500) < self.epsilon:
            moveIdx = random.randint(0, 2)
            newMove[moveIdx] = 1
        else:
            stateTensor = torch.tensor(state,dtype=torch.float)
            prediction = self.model(stateTensor)
            moveIdx = torch.argmax(prediction).item()
            newMove[moveIdx] = 1

        return newMove



    
def Train():
    plotScores = []
    plotMeanScores = []
    totalScore = 0
    bestScore = 0
    agent = Agent()
    game = Snake()
    if not RUN_FROM_EXISTING_MODEL:
        while True:
            # Get current state of game
            currentState = agent.GetState(game)

            # Get next move based on current state
            nextMove = agent.GetAction(currentState)

            #perform move and get new state
            reward, gameOver, score = game.PlayStep(nextMove)
            newState = agent.GetState(game)

            #train short memory
            agent.TrainShortMemory(currentState, nextMove, reward, newState, gameOver)

            #remember 
            agent.RememberData(currentState, nextMove, reward, newState, gameOver)

            if gameOver:
                #Train long term memory and plot result
                game.Reset()
                agent.numberOfGames += 1
                agent.TrainLongMemory()

                if score > bestScore:
                    bestScore = score
                    agent.model.Save(fileName="model_lr_0_0005_2.pth")  

                print('Game:', agent.numberOfGames, 'Score:', score, "Best score:", bestScore)
                plotScores.append(score)
                totalScore += score
                meanScore = totalScore/agent.numberOfGames
                plotMeanScores.append(meanScore)
                Plot(plotScores,plotMeanScores)
    else:
        agent.SetModel(MODEL_PATH)
        while True:
            currentState = agent.GetState(game)
            nextMove = agent.GetAction(currentState)
            reward, gameOver, score = game.PlayStep(nextMove)
            if gameOver:
                agent.numberOfGames += 1
                if score > bestScore:
                    bestScore = score
                game.Reset()
                print('Game:', agent.numberOfGames, 'Score:', score, "Best score:", bestScore)






if __name__ == "__main__":
    Train()
